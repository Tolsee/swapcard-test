const path = require('path');

module.exports = {
    entry: path.join(__dirname, 'src', 'web', 'react', 'index.js'),
    output: {
        filename: 'app.js',
        path: path.resolve(__dirname, 'lib', 'web', 'static', 'js')
    },
    devServer: {
        contentBase: path.resolve(__dirname, 'lib', 'web', 'static', 'js'),
        hot: true
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                loader: "babel-loader", // Do not use "use" here
                query: {
                    presets: ['react', 'es2015']
                }
            },
            {
                test: /\.less$/,
                use: [
                    "style-loader",
                    "css-loader",
                    "less-loader"
                ]
            },
            {
                test: /\.css/,
                loader: "style-loader!css-loader"
            }
        ],
    }
};