let mongoose = require('mongoose');

let chai = require('chai');
let chaiHttp = require('chai-http');

process.env.NODE_ENV = 'test';

let { server, User, Group, Task } = require('../lib/server');

let should = chai.should();

chai.use(chaiHttp);

describe('Users', (done) => {
    describe('/signup Users', () => {
        before(function (done) {
            User.remove({}).exec(function () {
                done();
            });
        });

        it('should not let sign up without user data.', (done) => {
            chai.request(server)
                .post('/api/signup')
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.success.should.be.false;
                    done();
                });
        });

        it('should validate username.', (done) => {
            let user = {
                username: 'asd$#',
                password: 'asjhdkjh123'
            };

            chai.request(server)
                .post('/api/signup')
                .send(user)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.success.should.be.false;
                    done();
                });
        });

        it('should validate password.', (done) => {
            let user = {
                username: 'TolseeSapkota123',
                password: 'asj'
            };

            chai.request(server)
                .post('/api/signup')
                .send(user)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.success.should.be.false;
                    done();
                });
        });

        it('should sign up a user.', (done) => {
            let user = {
                username: 'Tolsee123',
                password: 'aaasddds#$%'
            };

            chai.request(server)
                .post('/api/signup')
                .send(user)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.success.should.be.true;
                    res.body.should.have.property('token');
                    done();
                });
        });

        it('should not sign up users same username.', (done) => {
            let user = {
                username: 'Tolsee123',
                password: 'aaasddds#$%'
            };

            chai.request(server)
                .post('/api/signup')
                .send(user)
                .end((err, res) => {
                    res.should.have.status(409);
                    res.body.success.should.be.false;
                    done();
                });
        });
    });


    describe('/login', () => {
        it('should not login unknown user.', (done) => {
            let user = {
                username: 'Tolsee',
                password: 'aaasddds'
            };

            chai.request(server)
                .post('/api/login')
                .send(user)
                .end((err, res) => {
                    res.should.have.status(401);
                    res.body.success.should.be.false;
                    done();
                });
        });

        it('should login known user.', (done) => {
            let user = {
                username: 'Tolsee123',
                password: 'aaasddds#$%'
            };

            chai.request(server)
                .post('/api/login')
                .send(user)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.success.should.be.true;
                    res.body.should.have.property('token');
                    done();
                });
        });
    });


    describe('/logout', () => {
        let token;

        before(function (done) {
            let user = {
                username: 'Tolsee123',
                password: 'aaasddds#$%'
            };

            chai.request(server)
                .post('/api/login')
                .send(user)
                .end((err, res) => {
                    token = res.body.token;
                    done();
                });
        });

        it('it should logout without authorization', (done) => {
            chai.request(server)
                .get('/api/logout')
                .end((err, res) => {
                    res.should.have.status(401);
                    res.body.success.should.be.false;
                    done();
                });
        });

        it('it should logout user with authorization', (done) => {
            chai.request(server)
                .get('/api/logout')
                .set('Authorization', token)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.success.should.be.true;
                    done();
                });
        });
    });
});


describe('Tasks', () => {
    let token,
        taskId;
    before(function (done) {
        Task.remove({}).exec(function () {
            let user = {
                username: 'Tolsee123',
                password: 'aaasddds#$%'
            };

            chai.request(server)
                .post('/api/login')
                .send(user)
                .end((err, res) => {
                    token = res.body.token;
                    done();
                });
        });
    });


    describe('/POST Tasks', () => {
        it('should not allow post without token.', (done) => {
            let task = {
                title: 'Task for testing.',
                description: 'Test description.',
                dueDate: new Date('2018-02-02')
            };

            chai.request(server)
                .post('/api/task')
                .send(task)
                .end((err, res) => {
                    res.should.have.status(401);
                    res.body.success.should.be.false;
                    done();
                });
        });

        it('should not allow post without title.', (done) => {
            let task = {
                description: 'Test description.',
                dueDate: new Date('2018-02-02')
            };

            chai.request(server)
                .post('/api/task')
                .send(task)
                .set('Authorization', token)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.success.should.be.false;
                    done();
                });
        });

        it('should not allow post without dueDate.', (done) => {
            let task = {
                title: 'Task for testing.',
                description: 'Test description.',
            };

            chai.request(server)
                .post('/api/task')
                .send(task)
                .set('Authorization', token)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.success.should.be.false;
                    done();
                });
        });

        it('should allow post with token and data.', (done) => {
            let task = {
                title: 'Task for testing.',
                description: 'Test description.',
                dueDate: new Date('2018-02-02')
            };

            chai.request(server)
                .post('/api/task')
                .send(task)
                .set('Authorization', token)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.success.should.be.true;
                    res.body.should.have.property('data').which.should.be.a('object');
                    taskId = res.body.data._id;
                    done();
                });
        });

        it('should allow post with token and data. but not include invalid groupIds.', (done) => {
            let task = {
                title: 'Task for testing.',
                description: 'Test description.',
                dueDate: new Date('2018-02-02'),
                groupIds: ["invalidGroupId", "anotherWrongId"]
            };

            chai.request(server)
                .post('/api/task')
                .send(task)
                .set('Authorization', token)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.success.should.be.true;
                    res.body.should.have.property('data').which.should.be.a('object');
                    res.body.data.should.have.property('groupIds').lengthOf(0);
                    done();
                });
        });
    });

    describe('/GET a Task', () => {
        it('should not get without token.', (done) => {
            chai.request(server)
                .get(`/api/task/${taskId}`)
                .end((err, res) => {
                    res.should.have.status(401);
                    res.body.success.should.be.false;
                    done();
                });
        });

        it('should get data with invalid id.', (done) => {
            chai.request(server)
                .get('/api/task/invalidId')
                .set('Authorization', token)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.success.should.be.false;
                    done();
                });
        });

        it('should get data.', (done) => {
            chai.request(server)
                .get(`/api/task/${taskId}`)
                .set('Authorization', token)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.success.should.be.true;
                    res.body.should.have.property('data');
                    res.body.data.should.be.a('object');
                    done();
                });
        });
    });


    describe('/GET Tasks', () => {

        it('should not get without token.', (done) => {
            chai.request(server)
                .get(`/api/task/`)
                .end((err, res) => {
                    res.should.have.status(401);
                    res.body.success.should.be.false;
                    done();
                });
        });

        it('should get maximun of 100 taks.', (done) => {
            chai.request(server)
                .get('/api/task')
                .set('Authorization', token)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.success.should.be.true;
                    res.body.should.have.property('data');
                    res.body.data.should.be.a('array');
                    res.body.data.length.should.be.at.most(100);
                    done();
                });
        });

        it('should limit data.', (done) => {
            chai.request(server)
                .get(`/api/task`)
                .set('Authorization', token)
                .query({limit: 1})
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.success.should.be.true;
                    res.body.should.have.property('data');
                    res.body.data.should.be.a('array');
                    res.body.data.length.should.be.at.most(1);
                    done();
                });
        });
    });


    describe('/PUT Tasks', () => {
        let tokenOfDifferntUser;
        before(function (done) {
            let user = {
                username: 'newUser',
                password: 'aaasddds#$%'
            };

            chai.request(server)
                .post('/api/signup')
                .send(user)
                .end((err, res) => {
                    tokenOfDifferntUser = res.body.token;
                    done();
                });
        });

        it('should not put without token.', (done) => {
            let task = {
                title: 'Updated while testing.',
                description: 'Test description.',
                dueDate: new Date('2018-02-02')
            };

            chai.request(server)
                .put(`/api/task/${taskId}`)
                .send(task)
                .end((err, res) => {
                    res.should.have.status(401);
                    res.body.success.should.be.false;
                    done();
                });
        });

        it('should not let other users to put.', (done) => {
            let task = {
                title: 'Updated while testing.',
                description: 'Test description.',
                dueDate: new Date('2018-02-02')
            };

            chai.request(server)
                .put(`/api/task/${taskId}`)
                .send(task)
                .set('Authorization', tokenOfDifferntUser)
                .end((err, res) => {
                    res.should.have.status(401);
                    res.body.success.should.be.false;
                    done();
                });
        });

        it('should not put data with invalid id.', (done) => {
            let task = {
                title: 'Updated on put data invalid id.'
            };

            chai.request(server)
                .put('/api/task/invalidId')
                .set('Authorization', token)
                .send(task)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.success.should.be.false;
                    done();
                });
        });

        it('should put data.', (done) => {
            let task = {
                title: 'Updated on put data.',
                isDone: true
            };

            chai.request(server)
                .put(`/api/task/${taskId}`)
                .send(task)
                .set('Authorization', token)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.success.should.be.true;
                    res.body.data.should.have.property('title');
                    res.body.data.title.should.be.equal('Updated on put data.');
                    res.body.data.isDone.should.be.true;
                    done();
                });
        });

        it('should allow put but should not include invalid groupIds.', (done) => {
            let task = {
                title: 'Updated on put data invalid groupIds.',
                groupIds: ["invalidGroupId", "anotherWrongId"]
            };

            chai.request(server)
                .put(`/api/task/${taskId}`)
                .send(task)
                .set('Authorization', token)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.success.should.be.true;
                    res.body.data.should.have.property('title');
                    res.body.data.title.should.be.equal('Updated on put data invalid groupIds.')
                    res.body.data.should.have.property('groupIds').lengthOf(0);
                    done();
                });
        });

    });

    describe('/DELETE Tasks', () => {
        let tokenOfDifferntUser;
        before(function (done) {
            let user = {
                username: 'newUser',
                password: 'aaasddds#$%'
            };

            chai.request(server)
                .post('/api/login')
                .send(user)
                .end((err, res) => {
                    tokenOfDifferntUser = res.body.token;
                    done();
                });
        });

        it('should not detele without token.', (done) => {

            chai.request(server)
                .delete(`/api/task/${taskId}`)
                .end((err, res) => {
                    res.should.have.status(401);
                    res.body.success.should.be.false;
                    done();
                });
        });

        it('should not let other users to delete.', (done) => {
            chai.request(server)
                .delete(`/api/task/${taskId}`)
                .set('Authorization', tokenOfDifferntUser)
                .end((err, res) => {
                    res.should.have.status(401);
                    res.body.success.should.be.false;
                    done();
                });
        });

        it('should not delete with invalid id.', (done) => {
            chai.request(server)
                .delete('/api/task/invalidId')
                .set('Authorization', token)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.success.should.be.false;
                    done();
                });
        });

        it('should delete data.', (done) => {
            chai.request(server)
                .delete(`/api/task/${taskId}`)
                .set('Authorization', token)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.success.should.be.true;
                    done();
                });
        });
    });

    after(function () {
       server.close();
    });
});