import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import {
    Form,
    Input ,
    Button,
    Row,
    Col,
    Icon,
    message
} from 'antd';

import Auth from '../Auth';

import config from '../../../../config/client.json';

const FormItem = Form.Item;

class SignUp extends Component {
    constructor(props) {
        super(props);
        this.state = {
            confirmDirty: false
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.checkPassword = this.checkPassword.bind(this);
        this.checkConfirm = this.checkConfirm.bind(this);
        this.handleConfirmBlur  = this.handleConfirmBlur.bind(this);
    }

    handleSubmit(e){
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                fetch(`${config.serverUri}/api/signup`, {
                        method: 'POST',
                        headers: {
                            Accept: 'application/json',
                            'Content-Type': 'application/json',
                        },
                        body: JSON.stringify(values),
                    })
                    .then(res => res.json())
                    .then((res) => {
                        if (res.success) {
                            Auth.authenticateUser(res.token);
                            message.success(res.message);
                            window.location = '/';
                        } else {
                            message.error(res.message);
                        }
                    })
                    .catch(error => {
                        message.error('Login Failed! Please try again.');
                    });
            }
        });
    }

    handleConfirmBlur (e) {
        const value = e.target.value;
        this.setState({ confirmDirty: this.state.confirmDirty || !!value });
    }

    checkConfirm (rule, value, callback) {
        const form = this.props.form;
        if (value && this.state.confirmDirty) {
            form.validateFields(['rePassword'], { force: true });
        }
        callback();
    }

    checkPassword (rule, value, callback) {
        const form = this.props.form;
        if (value && value !== form.getFieldValue('password')) {
            callback('Please enter same password as above!');
        } else {
            callback();
        }
    }

    render() {

        const { getFieldDecorator } = this.props.form;
        return (
                <Row type="flex" justify="center" style={{padding: "100px 0", width: '100%'}}>
                    <Col span={8} >
                        <div>
                            <Form onSubmit={this.handleSubmit} >
                                <FormItem>
                                    {getFieldDecorator('username', {
                                        rules: [{ required: true, message: 'Please input your username!' }],
                                    })(
                                        <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Username" />
                                    )}
                                </FormItem>
                                <FormItem>
                                    {getFieldDecorator('password', {
                                        rules: [
                                            { required: true, message: 'Please input your password!' },
                                            { validator: this.checkConfirm}
                                            ],
                                    })(
                                        <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="Password" />
                                    )}
                                </FormItem>
                                <FormItem>
                                    {getFieldDecorator('rePassword', {
                                        rules: [
                                            { required: true, message: 'Re-enter your password!' },
                                            { validator: this.checkPassword}
                                        ],
                                    })(
                                        <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="Re-enter Password" onBlur={this.handleConfirmBlur}/>
                                    )}
                                </FormItem>
                                <FormItem>
                                    <Button type="primary" htmlType="submit" style={{display: 'flex', width: '100%', justifyContent: 'center'}}>
                                        Sign Up
                                    </Button>
                                    <div style={{display: 'flex', width: '100%', justifyContent: 'center'}}>
                                        Already have account? <div style={{width: 5}}> </div><Link to="/login"> Sign In!</Link>
                                    </div>
                                </FormItem>
                            </Form>
                        </div>
                    </Col>
                </Row>
        );
    }
}

export default Form.create()(SignUp);