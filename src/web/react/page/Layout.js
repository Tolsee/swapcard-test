import React, {Component} from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';

import { Layout } from 'antd';
import Login from './Login';
import Signup from './Signup';
import TaskList from './TaskList';
import NotFoundPage from './NotFoundPage';
import LogoutButton from '../component/LogoutButton';
import Loading from '../component/Loading';

import Auth from '../Auth';

const { Header, Footer, Content } = Layout;

class AppLayout extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: (typeof props.loading === 'undefined') ?  false : props.loading
        }
    }
    render() {
        return (
            <Layout style={{fontFamily: "sans-serif"}}>
                <Header style={{position: "fix", width: '100%', zIndex: 1000}}>
                    <div style={{ disply: "inline-flex", color: "#FFFFFF"}}>
                        Swapcard Test
                        <LogoutButton />
                    </div>
                </Header>

                <Content>
                    {this.state.loading ? <Loading minHeight='60vh' iconSize={45} fontSize={24}/> :
                        <Layout style={{padding: '0 50px', marginTop: 64}}>
                            <Switch>
                                <Route exact path="/" render={props => (
                                    Auth.isUserAuthenticated() ? (
                                        <TaskList {...props}/>
                                    ) : (<Redirect to={{
                                            pathname: '/login',
                                            state: {from: props.location}
                                        }}/>
                                    ))}/>
                                <Route path="/login" render={props => (
                                    Auth.isUserAuthenticated() ?
                                        (<Redirect to={{
                                                pathname: '/',
                                                state: {from: props.location}
                                            }}/>
                                        ) : (<Login/>))}/>
                                <Route path="/signup" render={props => (
                                    Auth.isUserAuthenticated() ?
                                        (<Redirect to={{
                                                pathname: '/',
                                                state: {from: props.location}
                                            }}/>
                                        ) : (<Signup/>))}/>
                                <Route component={NotFoundPage}/>
                            </Switch>
                        </Layout>
                    }
                </Content>

                <Footer style={{ textAlign: 'center' }}>
                    Express powered api with React frontend
                </Footer>
            </Layout>
        );
    }
}

export default AppLayout;