import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import Auth from '../Auth';

import config from '../../../../config/client.json';

import {Form, Icon, Input, Button, Row, Col, message} from 'antd';
const FormItem = Form.Item;

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit (e) {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                fetch(`${config.serverUri}/api/login`, {
                        method: 'POST',
                        headers: {
                            Accept: 'application/json',
                            'Content-Type': 'application/json',
                        },
                        body: JSON.stringify(values),
                    })
                    .then(res => res.json())
                    .catch(error => {
                        message.error('Login Failed! Please try again.');
                    })
                    .then((res) => {
                        if (res.success) {
                            Auth.authenticateUser(res.token);
                            message.success(res.message);
                            window.location = '/';
                        } else {
                            message.error(res.message);
                        }
                    });
            }
        });
    }

    render() {
        const { getFieldDecorator } = this.props.form;
        return (
            <Row type="flex" justify="center" style={{padding: "100px 0", width: '100%'}}>
                <Col span={8} >
                    <div>
                        <Form onSubmit={this.handleSubmit} >
                            <FormItem>
                                {getFieldDecorator('username', {
                                    rules: [{ required: true, message: 'Please input your username!' }],
                                })(
                                    <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="username" />
                                )}
                            </FormItem>
                            <FormItem>
                                {getFieldDecorator('password', {
                                    rules: [{ required: true, message: 'Please input your password!' }],
                                })(
                                    <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="Password" />
                                )}
                            </FormItem>
                            <FormItem>
                                <Button type="primary" htmlType="submit" className="login-form-button" style={{display: 'flex', width: '100%', justifyContent: 'center'}}>
                                    Log in
                                </Button>
                                <div style={{display: 'flex', width: '100%', justifyContent: 'center'}}>
                                    Or
                                </div>
                                <div style={{display: 'flex', width: '100%', justifyContent: 'center'}}>
                                    <Link to="/signup">Register now!</Link>
                                </div>
                            </FormItem>
                        </Form>
                    </div>
                </Col>
            </Row>
        );
    }
}

export default Form.create()(Login);