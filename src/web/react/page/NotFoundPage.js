import React, {Component} from 'react';
import { Link } from 'react-router-dom';
import { Layout, Row, Icon} from 'antd';

const { Content } = Layout;

class NotFoundPage extends Component {
    render() {
        return (
            <Content style={{ padding: '50px 24px', margin: 0, minHeight: 280, textAlign: "center" }}>
                <Row type='flex' justify='center'>
                    <Icon style={{ fontSize: 60, color: "#FFC107"}} type="smile-o" />
                </Row>
                <Row type='flex' justify='center' style={{ fontSize: 25, color: "#001529", fontFamily: "Roboto"}}>
                    Even If you lost somewhere,
                </Row>
                <Row type='flex' justify='center' style={{ fontSize: 18, color: "#424242", fontFamily: "sans-serif" }}>
                    You can always start from <Link to="/"> home.</Link>
                </Row>
            </Content>
        );
    }
}

export default NotFoundPage;