import React, {Component} from "react";
import {Card, Col, message, Row} from 'antd';

import config from '../../../../config/client.json';

import TaskListMenu from '../component/TaskListMenu';
import TaskCard from '../component/TaskCard';
import Loading from '../component/Loading';
import Auth from '../Auth';

export default class TaskList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            tasks: [],
            groups: [],
            loading: true
        };
        this.query = '';

        this.updateTaskList = this.updateTaskList.bind(this);
        this.updateGroupList = this.updateGroupList.bind(this);
        this.addGroup = this.addGroup.bind(this);
        this.addTask = this.addTask.bind(this);
        this.editTask = this.editTask.bind(this);
        this.deleteTask = this.deleteTask.bind(this);
        this.updateQuery = this.updateQuery.bind(this);
    }

    componentDidMount() {
        this.updateTaskList();
        this.updateGroupList();
    }

    updateGroupList() {
        fetch(`${config.serverUri}/api/group`,
            {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: Auth.getToken()
                }
            })
            .then(res => res.json())
            .then((res) => {
                if (res.success) {
                    this.setState({
                        groups: res.data,
                    });
                } else {
                    message.error(res.message);
                }
            })
            .catch(error => {
                message.error('Error occurred! Please try again.');
            });
    }

    updateTaskList() {
        this.setState({loading: true});
        fetch(`${config.serverUri}/api/task?${this.query}`,
            {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: Auth.getToken()
                }
            })
            .then(res => res.json())
            .then((res) => {
                if (res.success) {
                    this.setState({
                        tasks: res.data || [],
                        loading: false
                    });
                } else {
                    message.error(res.message);
                    this.setState({loading: false});
                }
            })
            .catch(error => {
                message.error('Error occurred! Please try again.');
                this.setState({loading: false});
            });
    }

    addGroup(group) {
        if (group._id && group.name) {
            let newGroupStates = [...this.state.groups, group];
            this.setState({
                groups: newGroupStates
            });
        }
    }

    addTask(task) {
        if (task._id && task.title && task.dueDate) {
            let newTaskStates = [...this.state.tasks, task];
            this.setState({
                tasks: newTaskStates
            });
        }
    }

    editTask(task) {
        if (task._id) {
            let  {tasks} = this.state;
            tasks = tasks.map((isEditedTask) => {
                return (isEditedTask._id == task._id) ?  task : isEditedTask;
            });
            this.setState({tasks: tasks});
        }
    }

    deleteTask(taskId) {
        let  {tasks} = this.state;
        tasks = tasks.filter((isDeletedTask) => (isDeletedTask._id != taskId));
        this.setState({tasks: tasks});
    }

    updateQuery(newQuery) {
        if (typeof newQuery === 'string') {
            this.query =  newQuery;
            this.updateTaskList();
        } else if (typeof newQuery === 'undefined' && this.query !== '') {
            this.query = '';
            this.updateTaskList();
        }
    }

    render() {
        return (
            <div>
                { this.state.loading ?
                    <Loading minHeight={200} iconSize={45} /> :
                    (
                        <div>
                            <TaskListMenu updateQuery={this.updateQuery} updateTaskList={this.updateTaskList} addTask={this.addTask} addGroup={this.addGroup} groups={this.state.groups} />
                            <Row type='flex' >
                                {
                                    this.state.tasks.length ?
                                        this.state.tasks.map((task) => (<TaskCard key={task._id} task={task} editTask={this.editTask} deleteTask={this.deleteTask} groups={this.state.groups}/>)) :
                                        (
                                            <Col span={16} offset={4}>
                                                <Card
                                                    style={{margin: "20px 0", width: '100%'}}
                                                >
                                                    <div style={{display: 'flex', justifyContent: 'center'}}>
                                                        <h4>No tasks found!</h4>
                                                    </div>
                                                </Card>
                                            </Col>
                                        )
                                }
                            </Row>
                        </div>
                    )
                }
            </div>
        );
    }
}