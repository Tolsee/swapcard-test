import React, {Component} from 'react';

import {
    Row,
    Col,
    Select
} from 'antd';

import AddTask from './AddTask';
import AddGroup from './AddGroup';

const {Option} = Select;

export default class TaskListMenu extends Component {
    constructor(props) {
        super(props);
        this.handleFilterChange = this.handleFilterChange.bind(this);
    }

    handleFilterChange(value) {
        if (value !== 'none' && value !== 'all') {
            let query = JSON.stringify({groupIds: value});

            this.props.updateQuery(`where=${query}`);
        } else {
            this.props.updateQuery(undefined);
        }
    }

    render() {
        return (
                <Row type='flex'>
                    <Col span={8} offset={4}>
                        <Row type='flex' justify="start" >
                            <Col span={6}>
                                <AddTask addTask={this.props.addTask}/>
                            </Col>
                            <Col span={6}>
                                <AddGroup addGroup={this.props.addGroup}/>
                            </Col>
                        </Row>
                    </Col>
                    <Col span={8}>
                        <Row type='flex' justify="end" >
                            <Col>
                                <Select  placeholder="Filter by Groups" style={{width: 150}} onChange={this.handleFilterChange}>
                                    <Option key='all' value='all'>All groups</Option>
                                    { this.props.groups ? this.props.groups.map((group) => <Option key={group._id} value={group._id}>{group.name}</Option>) : <Option value='none'>No groups</Option> }
                                </Select>
                            </Col>
                        </Row>
                    </Col>
                    <Col span={4} />
                </Row>
        );
    }
}
