import React, {Component} from 'react';
import {
    Modal,
    DatePicker,
    Form,
    Input,
    Icon,
    message
} from 'antd';
import moment from 'moment';

import Auth from '../Auth';

import config from '../../../../config/client.json';

const FormItem = Form.Item,
    {TextArea} = Input;

const EditTaskForm = Form.create()(
    (props) => {
        const { visible, handleCancel, handleOk, form, task} = props;
        const { getFieldDecorator } = form;
        return (
            <Modal
                title="Edit a Task"
                okText="Edit"
                visible={visible}
                onOk={handleOk}
                onCancel={handleCancel}
            >
                <Form layout="vertical">
                    <FormItem label="Title">
                        {getFieldDecorator('title', {
                            rules: [{ required: true, message: 'Please provide title of task!' }],
                            initialValue: task.title
                        })(
                            <Input placeholder="Title" />
                        )}
                    </FormItem>
                    <FormItem label="Description">
                        {getFieldDecorator('description', {
                            initialValue: task.description
                        })(
                            <TextArea placeholder="Description" />
                        )}
                    </FormItem>
                    <FormItem>
                        {getFieldDecorator('dueDate', {
                            rules: [{ required: true, message: 'Please provide due date!' }],
                            initialValue: moment(task.dueDate, '"YYYY-MM-DD HH:mm:ss')
                        })(
                            <DatePicker showTime format="YYYY-MM-DD HH:mm:ss" placeholder="Due Date"  />
                        )}
                    </FormItem>
                </Form>
            </Modal>
        );
    }
);

export default class EditTask extends Component {
    constructor(props) {
        super(props);
        this.state = {
            visible: false,
            taskId: props.task._id ? props.task._id : undefined
        };

        this.showModal = this.showModal.bind(this);
        this.handleOk = this.handleOk.bind(this);
        this.handleCancel = this.handleCancel.bind(this);
        this.saveFormRef = this.saveFormRef.bind(this);
    }

    showModal(e) {
        e.preventDefault();
        this.setState({
            visible: true
        });
    }

    handleOk(e) {
        e.preventDefault();
        if (!this.state.taskId) return;
        this.form.validateFields((err, values) => {
            if (!err) {
                fetch(`${config.serverUri}/api/task/${this.state.taskId}`,
                    {
                        method: 'PUT',
                        headers: {
                            Accept: 'application/json',
                            'Content-Type': 'application/json',
                            Authorization: Auth.getToken()
                        },
                        body: JSON.stringify(values)
                    })
                    .then(res => res.json())
                    .catch(error => {
                        message.error('Error occurred! Please try again.');
                    })
                    .then((res) => {
                        if (res.success) {
                            message.success(res.message);
                            this.setState({
                                visible: false,
                            });
                            this.props.editTask(res.data);
                        } else {
                            message.error(res.message);
                        }

                    });
            }
        });
    }

    handleCancel(e) {
        e.preventDefault();
        this.setState({
            visible: false,
        });
    }

    saveFormRef(form) {
        this.form = form;
    }

    render() {
        return (
            <div>
                <Icon type="edit" onClick={this.showModal}/>
                <EditTaskForm handleOk={this.handleOk} handleCancel={this.handleCancel} visible={this.state.visible} ref={this.saveFormRef} task={this.props.task}/>
            </div>
        );
    }
}

