import React from "react";
import {Modal} from 'antd';
import Auth from '../Auth';

import {Button} from 'antd';

let LogoutButton = () => {
    let logout = () => {
        Auth.deauthenticateUser();
    };
    return (
        <div style={{float: 'right'}}>
            {Auth.isUserAuthenticated() ? (<Button type="danger" onClick={logout} ghost>Logout</Button>) : undefined}
        </div>
    )
}

export default LogoutButton;