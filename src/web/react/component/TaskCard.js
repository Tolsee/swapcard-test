import React, { Component } from "react";
import {
    Card,
    Icon,
    Col,
    Divider,
    message
} from 'antd';

import Groups from './Groups';
import EditTask from './EditTask';
import DeleteTask from './DeleteTask';

import Auth from '../Auth';

import config from '../../../../config/client.json';

const {Meta} = Card;

export default class TaskCard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isDone: props.task.isDone
        };

        this.handleDone = this.handleDone.bind(this);
    }

    handleDone() {
        let task = this.props.task;
        fetch(`${config.serverUri}/api/task/${task._id}`,
            {
                method: 'PUT',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: Auth.getToken()
                },
                body: JSON.stringify({isDone: !task.isDone})
            })
            .then(res => res.json())
            .catch(error => {
                message.error('Error occurred! Please try again.');
            })
            .then((res) => {
                if (res.success) {
                    let msgText = res.data.isDone ? 'Done' : 'not Done'
                    message.success(`Task set to ${msgText}`);
                    this.setState({
                        isDone: res.data.isDone
                    });
                } else {
                    message.error(res.message);
                }

            });
    }

    render() {
        let {editTask, deleteTask, groups, task} = this.props;
        return (
            <Col span={16} offset={4}>
                <Card
                    style={{margin: "20px 0"}}
                    actions={[this.state.isDone ? (<Icon type="close" style={{color: '#C62828'}} onClick={this.handleDone}/>) : (<Icon type="check" style={{color: '#00E676'}} onClick={this.handleDone}/>), <EditTask editTask={editTask} task={task}/>, <DeleteTask taskId={task._id} deleteTask={deleteTask}/>]}
                >
                    <Meta
                        title={task.title}
                        description={task.description}
                    />
                    <Divider />
                    <h3>Groups (Please click to assign or unassign groups)</h3>
                    <Groups taskId={task._id} allGroups={groups} selectedGroups={task.groupIds}/>
                </Card>
            </Col>
        );
    }
}