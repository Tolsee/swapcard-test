import React from 'react';
import { Button, Modal, Form, Input, message } from 'antd';
const FormItem = Form.Item;

import config from '../../../../config/client.json';

import Auth from '../Auth';

const AddGroupForm = Form.create()(
    (props) => {
        const { visible, onCancel, onCreate, form } = props;
        const { getFieldDecorator } = form;
        return (
            <Modal
                visible={visible}
                title="Create a Group"
                okText="Add"
                onCancel={onCancel}
                onOk={onCreate}
            >
                <Form layout="vertical">
                    <FormItem label="Name">
                        {getFieldDecorator('name', {
                            rules: [{ required: true, message: 'Please provide title of task!' }],
                        })(
                            <Input placeholder="Title" />
                        )}
                    </FormItem>
                </Form>
            </Modal>
        );
    }
);

export default class AddGroup extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            visible: false
        };

        this.showModal = this.showModal.bind(this);
        this.handleOk = this.handleOk.bind(this);
        this.handleCancel = this.handleCancel.bind(this);
        this.saveFormRef = this.saveFormRef.bind(this);
    }
    showModal() {
        this.setState({ visible: true });
    }
    handleCancel() {
        this.setState({ visible: false });
    }
    handleOk(e) {
        e.preventDefault();
        this.form.validateFields((err, values) => {
            if (!err) {
                fetch(`${config.serverUri}/api/group`,
                    {
                        method: 'POST',
                        headers: {
                            Accept: 'application/json',
                            'Content-Type': 'application/json',
                            Authorization: Auth.getToken()
                        },
                        body: JSON.stringify(values)
                    })
                    .then(res => res.json())
                    .catch(error => {
                        message.error('Error occurred! Please try again.');
                    })
                    .then((res) => {
                        if (res.success) {
                            message.success(res.message);
                            this.setState({
                                visible: false,
                            });
                            this.props.addGroup(res.data);
                        } else {
                            message.error(res.message);
                        }

                    });
            }
        });
    }

    saveFormRef(form) {
        this.form = form;
    }

    render() {
        return (
            <div>
                <Button type="primary" icon="plus" onClick={this.showModal}>Group</Button>
                <AddGroupForm
                    ref={this.saveFormRef}
                    visible={this.state.visible}
                    onCancel={this.handleCancel}
                    onCreate={this.handleOk}
                />
            </div>
        );
    }
}