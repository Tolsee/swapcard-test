import React from 'react';
import {Icon, Col} from 'antd';

let Loading = (props) => {
    return (
        <Col style={{
            minHeight: props.minHeight,
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: '#f0f2f5'}}>
            <div style={{
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                justifyContent: 'center'}}>
                <Icon type="loading-3-quarters" spin={true} style={{ fontSize: props.iconSize, color: "#1976D2" }} />
                <div style={{fontSize: props.frontSize || 15 }}>Loading</div>
            </div>
        </Col>
    )
};

export default Loading;