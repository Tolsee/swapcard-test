import React from 'react';
import { Button, Modal, Form, Input, DatePicker, message } from 'antd';

import Auth from '../Auth';

import config from '../../../../config/client.json';

const FormItem = Form.Item,
    TextArea = Input.TextArea;



const AddTaskForm = Form.create()(
    (props) => {
        const { visible, onCancel, onCreate, form } = props;
        const { getFieldDecorator } = form;
        return (
            <Modal
                visible={visible}
                title="Add a new Task"
                okText="Add"
                onCancel={onCancel}
                onOk={onCreate}
            >
                <Form layout="vertical">
                    <FormItem label="Title">
                        {getFieldDecorator('title', {
                            rules: [{ required: true, message: 'Please provide title of task!' }],
                        })(
                            <Input placeholder="Title" />
                        )}
                    </FormItem>
                    <FormItem label="Description">
                        {getFieldDecorator('description')(
                            <TextArea placeholder="Description" />
                        )}
                    </FormItem>
                    <FormItem>
                        {getFieldDecorator('dueDate', {
                            rules: [{ required: true, message: 'Please provide due date!' }],
                        })(
                            <DatePicker
                                showTime
                                format="YYYY-MM-DD HH:mm:ss"
                                placeholder="Due Date"
                            />
                        )}
                    </FormItem>
                </Form>
            </Modal>
        );
    }
);

export default class AddTask extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            visible: false
        };

        this.showModal = this.showModal.bind(this);
        this.handleOk = this.handleOk.bind(this);
        this.handleCancel = this.handleCancel.bind(this);
        this.saveFormRef = this.saveFormRef.bind(this);
    }

    showModal() {
        this.setState({ visible: true });
    }

    handleCancel() {
        this.setState({ visible: false });
    }

    handleOk(e) {
        e.preventDefault();
        this.form.validateFields((err, values) => {
            if (!err) {
                fetch(`${config.serverUri}/api/task`,
                    {
                        method: 'POST',
                        headers: {
                            Accept: 'application/json',
                            'Content-Type': 'application/json',
                            Authorization: Auth.getToken()
                        },
                        body: JSON.stringify(values)
                    })
                    .then(res => res.json())
                    .catch(error => {
                        message.error('Error occurred! Please try again.');
                    })
                    .then((res) => {
                        if (res.success) {
                            message.success(res.message);
                            this.setState({
                                visible: false,
                            });
                            this.props.addTask(res.data);
                        } else {
                            message.error(res.message);
                        }

                    });
            }
        });
    }

    saveFormRef(form) {
        this.form = form;
    }

    render() {
        return (
            <div>
                <Button type="primary" icon="plus" onClick={this.showModal}>Task</Button>
                <AddTaskForm
                    ref={this.saveFormRef}
                    visible={this.state.visible}
                    onCancel={this.handleCancel}
                    onCreate={this.handleOk}
                />
            </div>
        );
    }
}