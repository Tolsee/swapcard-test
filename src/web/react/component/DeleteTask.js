import React, {Component} from 'react';
import {
    Modal,
    Icon,
    message
} from 'antd';

import Auth from '../Auth';

import config from '../../../../config/client.json';

const confirm = Modal.confirm;

export default class DeleteTask extends Component {
    constructor(props) {
        super(props);
        this.state = {
            taskId: props.taskId
        };

        this.handleClick = this.handleClick.bind(this);
    }

    handleClick(e) {
        e.preventDefault();
        let self = this;
        confirm({
            title: 'Are you sure delete this task?',
            content: 'This will delete your task forever.',
            okText: 'Yes',
            okType: 'danger',
            cancelText: 'No',
            onOk() {
                fetch(`${config.serverUri}/api/task/${self.state.taskId}`,
                    {
                        method: 'DELETE',
                        headers: {
                            Accept: 'application/json',
                            'Content-Type': 'application/json',
                            Authorization: Auth.getToken()
                        }
                    })
                    .then(res => res.json())
                    .catch(error => {
                        message.error('Error occurred! Please try again.');
                    })
                    .then((res) => {
                        if (res.success) {
                            message.success(res.message);
                            self.props.deleteTask(self.state.taskId);
                        } else {
                            message.error(res.message);
                        }

                    });
            }
        });

    }

    render() {
        return (
            <div>
                <Icon type="delete" onClick={this.handleClick}/>
            </div>
        );
    }
}

