import React, {Component} from 'react';
import { Tag, message } from 'antd';

import Auth from '../Auth';

import config from '../../../../config/client.json';

const { CheckableTag } = Tag;

export default class Groups extends Component {
    constructor(props) {
        super(props);
        this.state = {
            taskId: props.taskId,
            selectedGroups: props.selectedGroups
        };

        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(id) {
        let self = this;
        return function (checked) {
            let newStates = self.state.selectedGroups;
            if (checked) {
                newStates.push(self.props.allGroups.filter(ele => (ele._id == id))[0]);
            } else {
                newStates = newStates.filter(ele => (ele._id != id));
            }

            let selectedGroups = newStates.map(ele => ele._id);
            fetch(`${config.serverUri}/api/task/${self.state.taskId}`,
                {
                    method: 'PUT',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                        Authorization: Auth.getToken()
                    },
                    body: JSON.stringify({groupIds: selectedGroups})
                })
                .then(res => res.json())
                .catch(error => {
                    message.error('Error occurred! Please try again.');
                })
                .then((res) => {
                    if (res.success) {
                        let msgText = checked ? 'assigned to' : 'removed from';
                        message.success(`Task is ${msgText} a group`);
                        self.setState({selectedGroups: newStates});
                    } else {
                        message.error(res.message);
                    }
                });
        }
    }

    render() {
        let getGroups = () => {
                let Groups = (this.props.allGroups && this.props.allGroups.length) ? this.props.allGroups.map(allGroup => {
                    let isSelected = (this.state.selectedGroups && this.state.selectedGroups.length) ? this.state.selectedGroups.filter(selectedGroup => {
                        return selectedGroup._id == allGroup._id;
                    }) : false;

                    if (isSelected && isSelected.length) {
                        isSelected[0].checked = true;
                        return isSelected[0];
                    } else {
                        allGroup.checked = false;
                        return allGroup;
                    }
                }) : [];

                return Groups;
            },
            groups = getGroups();


        return (
            <div>
                {groups.length ? groups.map((group) => {
                    return (
                        <CheckableTag key={group._id} checked={group.checked} onChange={this.handleChange(group._id)}>{group.name}</CheckableTag>
                    );
                }) : <div></div>}
            </div>
        );
    }
}