import React from 'react';
import ReactDOM from 'react-dom';
import Layout from './page/Layout';

import {
    BrowserRouter as Router
} from 'react-router-dom';

window.onload = () => {
    ReactDOM.render((
        <Router>
            <Layout/>
        </Router>
    ), document.getElementById('app'));
};