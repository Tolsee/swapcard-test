import config from '../../../config/client.json';

export default {
    authenticateUser: (token) => {
        if (typeof localStorage === 'undefined') return;
        localStorage.setItem('token', token);
    },
    isUserAuthenticated: () => {
        if (typeof localStorage === 'undefined') return false;
        return localStorage.getItem('token') !== null;
    },
    deauthenticateUser: () => {
        if (typeof localStorage === 'undefined') return false;
        fetch(`${config.serverUri}/api/logout`, {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: localStorage.getItem('token')
                }
            })
            .then(res => res.json())
            .then((res) => {
                localStorage.removeItem('token');
                window.location.href = '/';
            });
    },
    getToken: () => {
        if (typeof localStorage === 'undefined') return false;
        return localStorage.getItem('token');
    }
}
