// ****************
// Import Libraries
// ****************
import jwt 	from 'jsonwebtoken';
import User from './models/Users';

let config = require(`../../config/${process.env.NODE_ENV}.json`);

export let checkUser = (req, res, next) => {
    if (!req.headers.authorization) {
        return res.status(401).json({
            success: false,
            message: "You are not authorized for this operation."
        });
    }

    let token = req.headers.authorization;

    try {
        let decoded = jwt.verify(token, config.jwtSecret),
            userId = decoded.id;

        return User.findOne({_id: userId, token: token}).exec()
            .then(function (user) {
                if (user) {
                    req.userId = user._id;
                    req.jwt = token;
                    next();
                } else {
                    return res.status(401).json({
                        success: false,
                        message: "You are not authorized for this operation."
                    })
                }
            })
            .catch(function (err) {
                return res.status(500).json({
                    success: false,
                    message: "Error occurred while processing. Please try again.",
                    err: err.message
                });
            });
    } catch (err) {
        return res.status(401).json({
            success: false,
            message: "You are not authorized for this operation.",
        });
    }
};

export let checkResourceAccess = (model) => {
    return (req, res, next) => {
        let userId = req.userId,
            _id = req.params.id;

        return model.findOne({userId, _id}).exec()
            .then(function (doc) {
                if (!doc) {
                    return res.status(401).json({
                        success: false,
                        message: "You are not authorized for this operation."
                    })
                } else {
                    return next();
                }
            })
            .catch(function (err) {
                if (err.name === 'CastError') {
                    return res.status(400).json({
                        success: false,
                        message: 'Please provide valid taskId.'
                    });
                }

                return res.status(500).json({
                    success: false,
                    message: "Error occurred while processing. Please try again.",
                    err: err.message
                });
            });
    };
};