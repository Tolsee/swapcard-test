import mongoose from 'mongoose';

let Schema = mongoose.Schema;

let UserSchema = new Schema({
    username: {type: String, required: true, unique: true},
    password: {type: String},
    token: [{type: String}],
    createdAt: {type: Date, default: new Date()},
});

export default mongoose.model('users', UserSchema);
