import mongoose from 'mongoose';

let Schema = mongoose.Schema;

let GroupSchema = new Schema({
    name: {type: String, required: true},
    userId: {type: Schema.Types.ObjectId, ref: 'users'},
    createdAt: {type: Date},
    updatedAt: {type: Date}
});

// Add createdAt, updatedAt in pre hooks
GroupSchema.pre('save', function(next) {
    var currentTime = new Date();

    this.updatedAt = currentTime;
    if (!this.createdAt) this.createdAt = currentTime;

    next();
});

GroupSchema.pre('update', function(next) {
    this._update.$set.updatedAt = new Date();
    next();
});

GroupSchema.pre('findOneAndUpdate', function(next) {
    this._update.$set.updatedAt = new Date();
    next();
});

export default mongoose.model('groups', GroupSchema);
