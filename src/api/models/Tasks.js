import mongoose from 'mongoose';

let Schema = mongoose.Schema;

let TaskSchema = new Schema({
    title: {type: String, required: true},
    description: {type: String},
    userId: {type: Schema.Types.ObjectId, ref: 'users', required: true},
    groupIds: [{type: Schema.Types.ObjectId, ref: 'groups'}],
    dueDate: {type: Date, required: true},
    isDone: {type: Boolean, default: false},
    createdAt: {type: Date},
    updatedAt: {type: Date}
});

// Add createdAt, updatedAt in pre hooks
TaskSchema.pre('save', function(next) {
    var currentTime = new Date();

    this.updatedAt = currentTime;
    if (!this.createdAt) this.createdAt = currentTime;

    next();
});

TaskSchema.pre('update', function(next) {
    this._update.$set.updatedAt = new Date();
    next();
});

TaskSchema.pre('findOneAndUpdate', function(next) {
    this._update.$set.updatedAt = new Date();
    next();
});

export default mongoose.model('tasks', TaskSchema);
