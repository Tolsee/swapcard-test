import mongoose from 'mongoose';
let config = require(`../../../config/${process.env.NODE_ENV}.json`);

let connect = () => {
    mongoose.connect(config.mongoUri, { config: { autoIndex: true } });

    // plug in the promise library:
    mongoose.Promise = global.Promise

    mongoose.connection.on('error', (err) => {
        console.error(`Mongoose connection error: ${err}`)
        process.exit(1)
    });
};

export default connect;
