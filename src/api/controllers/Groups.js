import Group from '../models/Groups';

export default {
    getGroup: function (userId, _id) {
        return Group.findOne({userId, _id}).exec();
    },
    getGroups: function (userId) {
        return Group.find({userId}).limit(100).exec();
    },
    postGroup: function (userId, group) {
        let newGroup = new Group({...group, userId});

        return newGroup.save();
    },
    putGroup: function (_id, newGroup) {
        return Group.findOneAndUpdate({_id}, {$set: newGroup}).exec();
    },
    deleteGroup: function (_id) {
        return Group.findOneAndUpdate({_id}).exec();
    }
};