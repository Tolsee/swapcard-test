import mongoose from 'mongoose';
import Task from '../models/Tasks';
import Group from '../models/Groups';

let checkGroups = function (userId, task) {
    return new Promise(function (resolve, reject) {
        if (task.groupIds && task.groupIds.length) {
            let groupIds;
            try {
                groupIds = typeof task.groupIds !== 'string' ? task.groupIds : JSON.parse(task.groupIds);
            } catch (err) {
                groupIds = [];
            }
            if (groupIds.length) {
                groupIds = groupIds.filter(groupId => mongoose.Types.ObjectId.isValid(groupId));
            }
            return Group.find({_id: {$in: groupIds}, userId}, function (err, groups) {
                if (err) {
                    reject(err);
                } else {
                    if (groups && groups.length) {
                        task.groupIds = groups.map(ele => ele._id);
                    } else {
                        delete task.groupIds;
                    }
                    return resolve(task);
                }
            });
        } else {
            return resolve(task);
        }
    });
};

export default {
    getTask: function (userId, id) {
        return Task.findOne({userId, _id: id}).populate('groupIds', 'name').exec();
    },
    getTasks: function (userId, query) {
        let retQuery = query.where ? Task.find({...JSON.parse(query.where), userId}).populate('groupIds', 'name') : Task.find({userId}).populate('groupIds', 'name');
        retQuery = query.skip ? retQuery.skip(parseInt(query.skip)) : retQuery;
        retQuery = query.limit ? retQuery.limit(parseInt(query.limit)) : retQuery.limit(100);

        return retQuery.exec();
    },
    postTask: function (userId, task) {
        return checkGroups(userId, task)
            .then(function (updatedTask) {
                let newTask = new Task({...updatedTask, userId});

                return newTask.save();
            });
    },
    putTask: function (userId, id, newDoc) {
        return checkGroups(userId, newDoc)
            .then(function (updatedTask) {
                return Task.findOneAndUpdate({_id: id}, {$set: updatedTask}, {new: true}).exec();
            });
    },
    deleteTask: function (id) {
        return Task.findOneAndRemove({_id: id}).exec();
    }
};