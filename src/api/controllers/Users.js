// ****************
// Import Libraries
// ****************
import jwt from 'jsonwebtoken';
import passwordHash from 'password-hash';

import User from '../models/Users';

let config = require(`../../../config/${process.env.NODE_ENV}.json`);

let createToken = function (user) {
    let payload = {
            user: user.username,
            id: user._id
        },
        token = jwt.sign(payload, config.jwtSecret);

    return User.findOneAndUpdate(
            {_id: user._id},
            {$push: {token}},
            {safe: true, new : true}
        ).exec()
        .then(function () {
            return token;
        });
};

export default {
    login: function (checkUser) {
        return User.findOne({username: checkUser.username}).exec()
            .then(function (user) {
                if (!user) {
                    const error = new Error('Incorrect email or password');
                    error.name = 'IncorrectCredentialsError';

                    throw error;
                }

                if(passwordHash.verify(checkUser.password, user.password)){
                    return createToken(user);
                } else {
                    let error = new Error('Incorrect password');
                    error.name = 'IncorrectCredentialsError';

                    throw error;
                }
            });
    },
    signup: function (user) {
        // Hash password
        user.password = passwordHash.generate(user.password);

        let newUser = new User(user);
        return newUser.save()
            .then(function (user) {
                return createToken(user);
            });
    },
    logout: function (userId, token) {
        return User.findOneAndUpdate(
            {_id: userId},
            {$pull: {token}},
            {safe: true, new : true}
        ).exec();
    }
};