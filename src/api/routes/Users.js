// ****************
// Import Libraries
// ****************
import express from 'express';

import userController from '../controllers/Users';

// ***************
// Router Handlers
// ***************
let roterHandlers = {
    login: function (req, res) {
        if (!req.body.username || !req.body.password) {
            return res.status(400).json({
                success: false,
                message: 'Please provide username and password.'
            });
        }

        return userController.login({username: req.body.username.trim(), password: req.body.password})
            .then(function (token) {
                return res.status(200).json({
                    success: true,
                    message: 'You have successfully logged in.',
                    token
                });
            })
            .catch(function (err) {
                if (err.name === 'IncorrectCredentialsError') {
                    return res.status(401).json({
                        success: false,
                        message: err.message
                    });
                }
                return res.status(500).json({
                    success: false,
                    message: err.message
                });
            });
    },
    signup: function (req, res) {
        if (!req.body.username || !req.body.password) {
            return res.status(400).json({
                success: false,
                message: 'Please provide username and password.'
            });
        }

        let usernameValidation = /[0-9a-zA-Z]{6,}$/,
            passwordValidation = /ˆ*.{8,}$/;

        if (!usernameValidation.test(req.body.username)) {
            return res.status(400).json({
                success: false,
                message: 'Please provide username with at least six alphanumeric characters.'
            });
        }

        if (!passwordValidation.test(req.body.password)) {
            return res.status(400).json({
                success: false,
                message: 'Please provide password with at least eight characters.'
            });
        }

        return userController.signup({username: req.body.username.trim(), password: req.body.password.trim()})
            .then(function (token) {
                res.status(200).json({
                    success: true,
                    message: 'You have successfully singed up.',
                    token
                });
            })
            .catch(function (err) {
                if (err.code === 11000) {
                    return res.status(409).json({
                        success: false,
                        message: 'Username is already taken. Please use another username.',
                    });
                }

                return res.status(500).json({
                    success: false,
                    message: 'Error occured while processing. Please try again.',
                    err: err.message
                });
            });
    },
    logout: function (req, res) {
        let userId = req.userId,
            token = req.jwt;
        return userController.logout(userId, token)
            .then(function () {
                return res.status(200).json({
                    success: true,
                    message: 'You have successfully logged out.',
                });
            })
            .catch(function (err) {
                return res.status(500).json({
                    success: false,
                    message: 'Error occured while processing. Please try again.',
                    err: err.message
                });
            });
    }
}
// ***************
// Router Handlers
// ***************


// **************
// Define Routers
// **************
let openRouter = express.Router(),
    userRouter = express.Router();

openRouter.post('/login', roterHandlers.login);
openRouter.post('/signup', roterHandlers.signup);
userRouter.get('/logout', roterHandlers.logout);
// **************
// Define Routers
// **************


// *************
// Export Router
// *************
export {
    openRouter,
    userRouter
};