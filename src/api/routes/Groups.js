// ****************
// Import Libraries
// ****************
import express from 'express';
import _ from 'underscore';

import Group from '../models/Groups';

import groupController from '../controllers/Groups';

import { checkResourceAccess } from '../middlewares';

// ***************
// Router Handlers
// ***************
let routerHandlers = {
    getGroup: function (req, res) {
        let groupId = req.params.id,
            userId = req.userId;

        return groupController.getGroup(userId, groupId)
            .then(function (group) {
                if (group) {
                    return res.status(200).json({
                        success: true,
                        message: 'You have successfully retrieved group.',
                        data: group
                    });
                }

                return res.status(200).json({
                    success: true,
                    message: 'No group found.'
                });
            })
            .catch(function (err) {
                return res.status(500).json({
                    success: false,
                    message: 'An error occurred while processing. Please try again.',
                    err: err.message
                });
            });
    },
    getGroups: function (req, res) {
        let userId = req.userId;

        return groupController.getGroups(userId)
            .then(function (groups) {
                if (groups.length) {
                    return res.status(200).json({
                        success: true,
                        message: 'You have successfully retrieved groups.',
                        data: groups
                    });
                }

                return res.status(200).json({
                    success: true,
                    message: 'No group found.'
                });
            })
            .catch(function (err) {
                return res.status(500).json({
                    success: false,
                    message: 'An error occurred while processing. Please try again.',
                    err: err.message
                });
            });

    },
    postGroup: function (req, res) {
        if (!req.body.name) {
            return res.status(400).json({
                success: true,
                message: 'Please provide name of the group.'
            });
        }
        let userId = req.userId,
            group = _.omit(req.body, 'userId');

        return groupController.postGroup(userId, group)
            .then(function (group) {
                return res.status(200).json({
                    success: true,
                    message: 'You have successfully saved a group.',
                    data: group
                });
            })
            .catch(function (err) {
                return res.status(500).json({
                    success: false,
                    message: 'An error occurred while processing. Please try again.',
                    err: err.message
                });
            });

    },
    putGroup: function (req, res) {
        let groupId = req.params.id,
            group = _.omit(req.body, 'userId');

        return groupController.putGroup(groupId, group)
            .then(function (group) {
                return res.status(200).json({
                    success: true,
                    message: 'You have successfully updated group.',
                    data: group
                });
            })
            .catch(function (err) {
                return res.status(500).json({
                    success: false,
                    message: 'An error occurred while processing. Please try again.',
                    err: err.message
                });
            });
    },
    deleteGroup: function (req, res) {
        let groupId = req.params.id;

        return groupController.deleteGroup(groupId)
            .then(function () {
                return res.status(200).json({
                    success: true,
                    message: 'You have successfully deleted group.'
                });
            })
            .catch(function (err) {
                return res.status(500).json({
                    success: false,
                    message: 'An error occurred while processing. Please try again.',
                    err: err.message
                });
            });

    }
};
// ***************
// Router Handlers
// ***************


// **************
// Define Routers
// **************
let router = express.Router();

router.get('/', routerHandlers.getGroups);
router.get('/:id', routerHandlers.getGroup);
router.post('/', routerHandlers.postGroup);
router.put('/:id', checkResourceAccess(Group), routerHandlers.putGroup);
router.delete('/:id', checkResourceAccess(Group), routerHandlers.deleteGroup);
// **************
// Define Routers
// **************


// *************
// Export Router
// *************
let exportRoutes = express.Router();
export default exportRoutes.use('/group', router);