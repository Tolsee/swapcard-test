// ****************
// Import Libraries
// ****************
import express from 'express';
import _ from 'underscore';

import Task from '../models/Tasks';

import taskController from '../controllers/Tasks';

import { checkResourceAccess } from '../middlewares';
// ***************
// Router Handlers
// ***************
let routerHandlers = {
    getTask: function (req, res) {
        let taskId = req.params.id,
            userId = req.userId;

        return taskController.getTask(userId, taskId)
            .then(function (task) {
                if (task) {
                    return res.status(200).json({
                        success: true,
                        message: 'You have successfully retrieved task.',
                        data: task
                    });
                }

                return res.status(200).json({
                    success: true,
                    message: 'No task found.'
                });
            })
            .catch(function (err) {
                if (err.name === 'CastError') {
                    return res.status(400).json({
                        success: false,
                        message: 'Please provide valid taskId.'
                    });
                }
                return res.status(500).json({
                    success: false,
                    message: 'An error occurred while processing. Please try again.',
                    err: err.message
                });
            });

    },
    getTasks: function (req, res) {
        let userId = req.userId,
            query = _.pick(req.query || {}, 'limit', 'skip', 'sort', 'where');

        return taskController.getTasks(userId, query)
            .then(function (tasks) {
                if (tasks.length) {
                    return res.status(200).json({
                        success: true,
                        message: 'You have successfully retrieved tasks.',
                        data: tasks
                    });
                }

                return res.status(200).json({
                    success: true,
                    message: 'No task found.'
                });
            })
            .catch(function (err) {
                return res.status(500).json({
                    success: false,
                    message: 'An error occurred while processing. Please try again.',
                    err: err.message
                });
            });
    },
    postTask: function (req, res) {
        if (!req.body.title || !req.body.dueDate) {
            return res.status(400).json({
                success: false,
                message: 'Please provide at least title and dueDate'
            });
        }

        let userId = req.userId,
            task = _.omit(req.body, 'userId');

        return taskController.postTask(userId, task)
            .then(function (task) {
                return res.status(200).json({
                    success: true,
                    message: 'You have successfully saved a task.',
                    data: task
                });
            })
            .catch(function (err) {
                return res.status(500).json({
                    success: false,
                    message: 'An error occurred while processing. Please try again.',
                    err: err.message
                });
            });
    },
    putTask: function (req, res) {
        let userId = req.userId,
            taskId = req.params.id,
            newTask = _.omit(req.body, 'userId');

        return taskController.putTask(userId, taskId, newTask)
            .then(function (task) {
                return res.status(200).json({
                    success: true,
                    message: 'You have successfully updated task.',
                    data: task
                });
            })
            .catch(function (err) {
                return res.status(500).json({
                    success: false,
                    message: 'An error occurred while processing. Please try again.',
                    err: err.message
                });
            });
    },
    deleteTask: function (req, res) {
        let taskId = req.params.id;

        return taskController.deleteTask(taskId)
            .then(function () {
               return res.status(200).json({
                  success: true,
                  message: 'You have successfully deleted task'
               });
            })
            .catch(function (err) {
                return res.status(500).json({
                    success: false,
                    message: 'An error occurred while processing. Please try again.',
                    err: err.message
                });
            });
    }
};
// ***************
// Router Handlers
// ***************


// **************
// Define Routers
// **************
let router = express.Router();

router.get('/', routerHandlers.getTasks);
router.get('/:id', routerHandlers.getTask);
router.post('/', routerHandlers.postTask);
router.put('/:id', checkResourceAccess(Task), routerHandlers.putTask);
router.delete('/:id', checkResourceAccess(Task), routerHandlers.deleteTask);

// **************
// Define Routers
// **************


// *************
// Export Router
// *************
let exportRoutes = express.Router();
export default exportRoutes.use('/task', router);