import express from 'express';

import groupRouter from './Groups';
import taskRouter from './Tasks';
import {openRouter, userRouter} from './Users';

import {checkUser} from '../middlewares';

let apiRouter = express.Router();

apiRouter.use('/', openRouter);
apiRouter.use('/', checkUser, groupRouter);
apiRouter.use('/', checkUser, taskRouter);
apiRouter.use('/', checkUser, userRouter);

export default apiRouter;




