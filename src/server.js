import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import path from 'path';

import apiRouter from './api/routes/index';

import reactRender from './web/router';

// Import Models for testing
import User from './api/models/Users';
import Group from './api/models/Groups';
import Task from './api/models/Tasks';

// Connect mongoDB
import initMongoDB from './api/models/index';
initMongoDB();

let config = require(`../config/${process.env.NODE_ENV}.json`);

var app = express();


// Enable Cross Origin Requests
app.use(cors());


// Parse the HTTP request
app.use(bodyParser.urlencoded({ extended: true, limit: '500mb'}))
app.use(bodyParser.json())

app.use('/api', apiRouter);

/*
* Render Frontend
* */

app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'web', 'views'));

// define the folder that will be used for static assets
app.use(express.static(path.join(__dirname, 'web', 'static')));

app.use('/', reactRender);

let server = app.listen(config.port, () => {
    console.log(`Server running at port ${config.port}`);
});

// For testing
module.exports = {
    server,
    User,
    Group,
    Task
};
