
# Swapcard Test : Express Api with React Front-end

## About

This serves as a test work for swapcard by Tulsi Sapkota
### Technology
* Express.js for RESTful API
* MongoDB, mongoose.js 
* React.js

## Installation
### Install Dependencies
```bash
npm install
```
### Build Server
```bash
npm run buildServer 
```
### Build Front-end
```bash
npm run buildClient
```

## Running Dev Server

```bash
npm run dev
```
## Testing
```bash
npm run test
```
## Building and Running Production Server

```bash
npm run build
npm run serve
```

## Demo
http://swpcrd.starsworthy.com/

---
Thanks for checking this out.

� Tulsi Sapkota